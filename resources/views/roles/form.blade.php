<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
        </div>
    </div>



    <div class="form-group">
        <label for="sel1">Permissions:</label>
        <select class="form-control" multiple name="permission[]">
            @foreach($permissions as $permission)
            <option value="{{$permission->id}}">{{$permission->name}}</option>
            @endforeach
        </select>
    </div>




{{--    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Permissions:</strong>

            {!!
            Form::select('size', [$permission->id => $permission->name])
/*            Form::select('name', null, array('placeholder' => 'Name','class' => 'form-control'))
*/             !!}

        </div>
    </div>--}}

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>